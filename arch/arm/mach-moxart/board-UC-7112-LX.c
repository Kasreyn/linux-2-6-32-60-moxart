/* Copyright (C) 2013 Jonas Jensen <jonas.jensen@gmail.com>
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version. */

#include <linux/kernel.h>
#include <linux/init.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>

#include <mach/board.h>

static void __init moxart_init(void)
{
	moxart_register_uart();
	moxart_add_ethernet_devices();
	moxart_add_rtc_device();
	moxart_add_dma_device();
}

static void __init fixup_uc711x(struct machine_desc *desc,
	struct tag *tags, char **cmdline, struct meminfo *mi)
{
	/* moxa_serial_init(); */
}

MACHINE_START(MOXART, "MOXA UC-7112-LX")
	.fixup			= fixup_uc711x,
	.map_io			= moxart_map_io,
	.init_machine	= moxart_init,
	.init_irq		= moxart_init_irq,
	.timer			= &moxart_timer,

/*	.boot_params	= 0x100,
	.atag_offset	= 0x100,
	.param_offset   = 0x00000100, */
MACHINE_END


