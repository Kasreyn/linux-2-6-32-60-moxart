/* Copyright (C) 2013 Jonas Jensen <jonas.jensen@gmail.com>
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version. */

#ifndef __MACH_IRQS_H__
#define __MACH_IRQS_H__

#define IRQ_UART	31
#define IRQ_TIMER1	19
#define IRQ_TIMER2	14
#define IRQ_TIMER3	15
#define IRQ_MAC1	25
#define IRQ_MAC2	27
#define IRQ_MMC		5
#define IRQ_APB_DMA	24

/*
 #define IRQ_SERIRQ1	11
 #define IRQ_SERIRQ0	10
 #define IRQ_PWR		26
 #define IRQ_CIR		25
 #define IRQ_RTC		17
 #define IRQ_FLASH	12
 #define IRQ_DMA		9
 #define IRQ_WATCHDOG	3
 #define IRQ_IPI		0
 */

#define MOXART_NR_IRQS		32
#define MOXART_NR_FIQS		32
#define NR_IRQS				(MOXART_NR_IRQS + MOXART_NR_FIQS + 16)

#define LEVEL				0
#define EDGE				1
#define H_ACTIVE			0
#define L_ACTIVE			1

#define IRQ_SOURCE_REG		0
#define IRQ_MASK_REG		0x04
#define IRQ_CLEAR_REG		0x08
#define IRQ_MODE_REG		0x0c
#define IRQ_LEVEL_REG		0x10
#define IRQ_STATUS_REG		0x14

#define FIQ_SOURCE_REG		0x20
#define FIQ_MASK_REG		0x24
#define FIQ_CLEAR_REG		0x28
#define FIQ_MODE_REG		0x2c
#define FIQ_LEVEL_REG		0x30
#define FIQ_STATUS_REG		0x34

#define IRQ_SOURCE(base_addr)	(base_addr + 0x00)
#define IRQ_MASK(base_addr)		(base_addr + 0x04)
#define IRQ_CLEAR(base_addr)	(base_addr + 0x08)
#define IRQ_TMODE(base_addr)	(base_addr + 0x0C)
#define IRQ_TLEVEL(base_addr)	(base_addr + 0x10)
#define IRQ_STATUS(base_addr)	(base_addr + 0x14)
#define FIQ_SOURCE(base_addr)	(base_addr + 0x20)
#define FIQ_MASK(base_addr)		(base_addr + 0x24)
#define FIQ_CLEAR(base_addr)	(base_addr + 0x28)
#define FIQ_TMODE(base_addr)	(base_addr + 0x2C)
#define FIQ_TLEVEL(base_addr)	(base_addr + 0x30)
#define FIQ_STATUS(base_addr)	(base_addr + 0x34)

#ifndef __ASSEMBLY__

extern void moxart_int_set_irq(unsigned int irq, int mode, int level);

#endif

#endif /* __MACH_IRQS_H__ */
