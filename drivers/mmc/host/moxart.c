/* MOXART MMC/SD device driver (based on MOXA sources)
 * Copyright (C) 2013 Jonas Jensen <jonas.jensen@gmail.com>
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version. */

#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/ioport.h>
#include <linux/platform_device.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/blkdev.h>
#include <linux/dma-mapping.h>
#include <linux/mmc/host.h>
#include <linux/sched.h>
#include <linux/mmc/sd.h>
#include <linux/io.h>

#include <asm/dma.h>
#include <asm/irq.h>
#include <asm/sizes.h>

#include <mach/apb_dma.h>
#include <mach/irqs.h>
#include <mach/hardware.h>
#include <mach/gpio.h>

#include "moxart.h"

static inline void moxart_init_sg(struct moxart_host *host,
	struct mmc_data *data)
{
	/* Get info. about SG list from data structure. */
	host->cur_sg = data->sg;
	host->num_sg = data->sg_len;
	host->remain = host->cur_sg->length;

	if (host->remain > host->size)
		host->remain = host->size;
	host->mapped_sg = NULL;

	data->error = MMC_ERR_NONE;
}

static inline int moxart_next_sg(struct moxart_host *host)
{
	int remain;
	struct mmc_data *data = host->data;

	host->cur_sg++;
	host->num_sg--;

	if (host->num_sg > 0) {
		host->remain = host->cur_sg->length;
		remain = host->size - data->bytes_xfered;
		if (remain > 0 && remain < host->remain)
			host->remain = remain;
	}

	return host->num_sg;
}

static void moxart_do_fifo(struct moxart_host *host, struct mmc_data *data)
{
	char *buffer;
	int wcnt, i;

	if (host->size == data->bytes_xfered)
		return;

	/* local_irq_save(kmap_flags); */
	/* dbg_printk(KERN_INFO "moxart_do_fifo: kmap_atomic\n"); */
	host->mapped_sg = kmap_atomic(sg_page(host->cur_sg), KM_BIO_SRC_IRQ);
	buffer = host->mapped_sg + host->cur_sg->offset;

	if (host->size > MSD_FIFO_LENB && host->dma) {
		struct apb_dma_conf_param param;
		param.size = host->remain;
		param.burst_mode = APB_DMAB_BURST_MODE;
		param.data_width = APB_DMAB_DATA_WIDTH_4;
		if (data->flags & MMC_DATA_WRITE) {
			param.source_addr = (unsigned int) buffer;
			param.dest_addr = (unsigned int)
				&host->reg->data_window;
			param.dest_inc = APB_DMAB_DEST_INC_0;
			param.source_inc = APB_DMAB_DEST_INC_4_16;
			param.dest_sel = APB_DMAB_DEST_APB;
			param.source_sel = APB_DMAB_SOURCE_AHB;
		} else {
			param.dest_addr = (unsigned int) buffer;
			param.source_addr = (unsigned int)
				&host->reg->data_window;
			param.source_inc = APB_DMAB_DEST_INC_0;
			param.dest_inc = APB_DMAB_DEST_INC_4_16;
			param.source_sel = APB_DMAB_DEST_APB;
			param.dest_sel = APB_DMAB_SOURCE_AHB;
		}
		data->bytes_xfered += host->remain;
		apb_dma_conf(host->dma, &param);
		/*printk(KERN_INFO "moxart_do_fifo: apb_dma_enable\n");*/
		apb_dma_enable(host->dma);
	} else {
		/*printk(KERN_INFO "moxart_do_fifo: PIO transfer\n");*/
		wcnt = host->remain >> 2;
		if (data->flags & MMC_DATA_WRITE) {
			/* dbg_printk(KERN_INFO "moxart_do_fifo:"
			 *		" MMC_DATA_WRITE)\n"); */
			for (i = 0; i < wcnt; i++, buffer += 4) {
				writel(*(unsigned int *)buffer,
					&host->reg->data_window);
			}
		} else {
			/* dbg_printk(KERN_INFO "moxart_do_fifo:"
			 *		" MMC_DATA_READ)\n"); */
			for (i = 0; i < wcnt; i++, buffer += 4) {
				*(unsigned int *)buffer = readl(
					&host->reg->data_window);
			}
		}
		wcnt <<= 2;
		host->remain -= wcnt;
		data->bytes_xfered += wcnt;
	}

	/* dbg_printk(KERN_INFO "moxart_do_fifo: kunmap_atomic\n"); */
	kunmap_atomic(host->mapped_sg, KM_BIO_SRC_IRQ);
	moxart_next_sg(host);
	/* local_irq_restore(kmap_flags); */
}

static void moxart_request_done(struct moxart_host *host)
{
	struct mmc_request *mrq = host->mrq;

	if (mrq == NULL)
		return;

	host->mrq = NULL;
	host->data = NULL;
	mmc_request_done(host->mmc, mrq);
}

static void moxart_prepare_data(struct moxart_host *host, struct mmc_data *data)
{
	unsigned int timeout, datactrl;
	int blksz_bits;

	host->data = data;
	host->size = data->blocks * data->blksz;
	blksz_bits = ffs(data->blksz) - 1;
	BUG_ON(1 << blksz_bits != data->blksz);

	moxart_init_sg(host, data);

	timeout = (host->mmc->f_max / 1000) * (data->timeout_ns / 1000);
	timeout *= 2;

	datactrl = (blksz_bits & MSD_BLK_SIZE_MASK) | MSD_DATA_EN;
	if (data->flags & MMC_DATA_WRITE)
		datactrl |= MSD_DATA_WRITE;
	if (host->size > MSD_FIFO_LENB && host->dma)
		datactrl |= MSD_DMA_EN;
	writel(timeout, &host->reg->data_timer);
	writel(host->size, &host->reg->data_length);
	writel(datactrl, &host->reg->data_control);

	if (host->size > MSD_FIFO_LENB && host->dma) {
		writel(MSD_INT_CARD_CHANGE, &host->reg->interrupt_mask);
		/* dbg_printk(KERN_INFO "moxart_prepare_data:"
		 *	" calling moxart_do_fifo\n"); */
		moxart_do_fifo(host, data);
		/* tasklet_schedule(&host->fifo_run_tasklet); */
	} else {
		/* dbg_printk(KERN_INFO "moxart_prepare_data: calling writel("
		 *	"MSD_INT_FIFO_URUN | MSD_INT_FIFO_ORUN |
			MSD_INT_CARD_CHANGE,
			&host->reg->interrupt_mask); \n"); */
		writel(MSD_INT_FIFO_URUN | MSD_INT_FIFO_ORUN |
			MSD_INT_CARD_CHANGE,
			&host->reg->interrupt_mask);
	}
}

static void moxart_send_command(struct moxart_host *host,
	struct mmc_command *cmd)
{
	unsigned int status, cmdctrl;
	int retry = 0;

	/* dbg_printk(KERN_INFO "moxart_send_command\n"); */
	cmd->error = MMC_ERR_TIMEOUT;
	writel(MSD_CLR_RSP_TIMEOUT | MSD_CLR_RSP_CRC_OK |
		MSD_CLR_RSP_CRC_FAIL | MSD_CLR_CMD_SENT, &host->reg->clear);
	writel(cmd->arg, &host->reg->argument);

	/* same info provided by CONFIG_MMC_DEBUG (when it actually works) */
	/* dbg_printk(KERN_INFO "moxart_send_command: cmd->opcode=%d",
		cmd->opcode); */

	cmdctrl = cmd->opcode & MSD_CMD_IDX_MASK;
	if (cmdctrl == SD_APP_SET_BUS_WIDTH || cmdctrl == SD_APP_OP_COND ||
		cmdctrl == SD_APP_SEND_SCR)  {
		/* this is SD application specific command */
		cmdctrl |= MSD_APP_CMD;
	}

	if (cmd->flags & MMC_RSP_136)
		cmdctrl |= (MSD_LONG_RSP | MSD_NEED_RSP);
	else
		cmdctrl |= MSD_NEED_RSP;

	/* dbg_printk(KERN_INFO " cmdctrl=%d\n", cmdctrl); */
	writel(cmdctrl | MSD_CMD_EN, &host->reg->command);

	while (retry++ < MSD_RETRY_COUNT) {	/* wait response */
		status = readl(&host->reg->status);
		if (status & MSD_CARD_DETECT) { /* card is removed */
			cmd->error = MMC_ERR_TIMEOUT;
			break;
		}
		if (cmdctrl & MSD_NEED_RSP) {
			if (status & MSD_RSP_TIMEOUT) {
				writel(MSD_CLR_RSP_TIMEOUT, &host->reg->clear);
				cmd->error = MMC_ERR_TIMEOUT;
				break;
			}
			/* if ( status & MSD_RSP_CRC_FAIL ) { */
			if ((cmd->flags & MMC_RSP_CRC) &&
				(status & MSD_RSP_CRC_FAIL)) {
				writel(MSD_CLR_RSP_CRC_FAIL, &host->reg->clear);
				cmd->error = MMC_ERR_BADCRC;
				break;
			}
			if (status & MSD_RSP_CRC_OK) {
				writel(MSD_CLR_RSP_CRC_OK, &host->reg->clear);
				cmd->resp[0] = readl(&host->reg->response0);
				cmd->resp[1] = readl(&host->reg->response1);
				cmd->resp[2] = readl(&host->reg->response2);
				cmd->resp[3] = readl(&host->reg->response3);
				/*
				 * following dbg_printk "fixes" commit <
				 * 16326b589a33c5a70bc53381c9096aa8fc1eaafc
				 * (because of commented spin_lock_irqsave:s)
				 * when kmap_atomic is followed up with
				 * kunmap_atomic:
				 * spin_lock_irqsave:s can be re-enabled without
				 * warnings and locking issues.
				 * dbg_printk(KERN_INFO "moxart_send_command"
				 *		" response:"
				 *		" %x:%x:%x:%x\n", cmd->resp[0],
				 *		cmd->resp[1], cmd->resp[2],
				 *		cmd->resp[3]);
				 */
				cmd->error = MMC_ERR_NONE;
				break;
			}
		} else {
			if (status & MSD_CMD_SENT) {
				writel(MSD_CLR_CMD_SENT, &host->reg->clear);
				cmd->error = MMC_ERR_NONE;
				break;
			}
		}
	}
}

static irqreturn_t moxart_irq(int irq, void *devid)
{
	struct moxart_host *host = devid;
	unsigned int status;

	status = readl(&host->reg->status); /* get the interrupt status */
	if (status & MSD_CARD_CHANGE) { /* has card inserted or removed */
		/* writel(MSD_CLR_CARD_CHANGE, &host->reg->clear); */
		/* dbg_printk(KERN_INFO "moxart_irq: status &"
		 *		" MSD_CARD_CHANGE\n"); */
		tasklet_schedule(&host->card_change_tasklet);
	}
	if (status & (MSD_FIFO_ORUN | MSD_FIFO_URUN)) {
		/* dbg_printk(KERN_INFO "moxart_irq: status &"
		 *		" (MSD_FIFO_ORUN | MSD_FIFO_URUN)\n"); */
		writel(status & (MSD_FIFO_ORUN | MSD_FIFO_URUN),
			&host->reg->clear);
		tasklet_schedule(&host->fifo_run_tasklet);
	}

	return IRQ_HANDLED;
}

static void moxart_fifo_run(unsigned long param)
{
	struct moxart_host *host = (struct moxart_host *) param;
	struct mmc_data *data;
	unsigned long flags;

	host = (struct moxart_host *) param;
	data = host->data;
	if (host->mrq == NULL || data == NULL) {
		/* spin_unlock_irqrestore(&host->lock, flags); */
		return;
	}

	/* dbg_printk(KERN_INFO "moxart_fifo_run:"
	 *		" moxart_do_fifo(host, data)\n"); */
	moxart_do_fifo(host, data);

	spin_lock_irqsave(&host->lock, flags);

	if (host->size == data->bytes_xfered) {
		unsigned int status;
		while (1) {
			status = readl(&host->reg->status);
			if (status & (MSD_DATA_CRC_OK | MSD_DATA_CRC_FAIL |
				MSD_DATA_END))
				break;
			schedule_timeout_interruptible(5);
		}

		if (status & MSD_DATA_CRC_OK)
			writel(MSD_CLR_DATA_CRC_OK, &host->reg->clear);

		if (status & MSD_DATA_CRC_FAIL) {
			writel(MSD_CLR_DATA_CRC_FAIL, &host->reg->clear);
			data->error = MMC_ERR_TIMEOUT;
		}

		if (status & MSD_DATA_END)
			writel(MSD_CLR_DATA_END, &host->reg->clear);

		if (data->stop)
			moxart_send_command(host, data->stop);

	} else {
		spin_unlock_irqrestore(&host->lock, flags);
		return;
	}

	spin_unlock_irqrestore(&host->lock, flags);
	moxart_request_done(host);
}

static void moxart_card_change(unsigned long param)
{
	struct moxart_host *host = (struct moxart_host *) param;
	unsigned int status;
	int delay;
	unsigned long flags;

	spin_lock_irqsave(&host->lock, flags);
	status = readl(&host->reg->status);

	if (status & MSD_CARD_DETECT) {
		dbg_printk(KERN_INFO "MOXART MMC/SD card removed.\n");
		delay = 0;
		if (host->data) {

			if (host->dma && host->size > MSD_FIFO_LENB)
				apb_dma_disable(host->dma);

			host->size = host->data->bytes_xfered;
			moxart_fifo_run(*(unsigned long *) host);
			host->data->error = MMC_ERR_TIMEOUT;
			moxart_request_done(host);
		}

		if (host->mrq) {
			host->mrq->cmd->error = MMC_ERR_TIMEOUT;
			moxart_request_done(host);
		}

	} else {
		dbg_printk(KERN_INFO "MOXART MMC/SD card inserted.\n");
		delay = 500;
	}

	writel(MSD_CLR_CARD_CHANGE, &host->reg->clear);
	spin_unlock_irqrestore(&host->lock, flags);
	mmc_detect_change(host->mmc, msecs_to_jiffies(delay));
}

static void moxart_dma_irq(void *param)
{
	struct moxart_host *host = (struct moxart_host *) param;

	/*printk(KERN_INFO "moxart_dma_irq\n");*/
	if (host)
		tasklet_schedule(&host->fifo_run_tasklet);
}

static void moxart_request(struct mmc_host *mmc, struct mmc_request *mrq)
{
	struct moxart_host *host = mmc_priv(mmc);
	struct mmc_command *cmd;
	unsigned long flags;

	/* dbg_printk(KERN_INFO "moxart_request\n"); */

	spin_lock_irqsave(&host->lock, flags);

	host->mrq = mrq;
	cmd = mrq->cmd;

	if (readl(&host->reg->status) & MSD_CARD_DETECT) {
		/* card is removed if no card inserted, return timeout error */
		cmd->error = MMC_ERR_TIMEOUT;
		goto request_done;
	}

	if (cmd->data) { /* request include data or not */
		/* dbg_printk(KERN_INFO "moxart_request: cmd->data\n"); */
		moxart_prepare_data(host, cmd->data);
	}

	moxart_send_command(host, cmd);

	if (cmd->data && cmd->error == MMC_ERR_NONE) {
		spin_unlock_irqrestore(&host->lock, flags);
		/* only added this to avoid lockups/warnings when DMA interrupt
		 * was broken (it really should not be done here) */
		/* moxart_request_done(host); */
		return;
	}

request_done:
	spin_unlock_irqrestore(&host->lock, flags);
	moxart_request_done(host);
}

#define MIN_POWER	(MMC_VDD_360 - MSD_SD_POWER_MASK)
static void moxart_set_ios(struct mmc_host *mmc, struct mmc_ios *ios)
{
	struct moxart_host *host = mmc_priv(mmc);
	unsigned long flags;
	unsigned short power;

	spin_lock_irqsave(&host->lock, flags);
	if (ios->clock) {
		int div;
#ifdef MSD_SUPPORT_GET_CLOCK
		div = (host->sysclk / (host->mmc->f_max * 2)) - 1;
#else
		div = (APB_CLK / (host->mmc->f_max * 2)) - 1;
#endif

		if (div > MSD_CLK_DIV_MASK)
			div = MSD_CLK_DIV_MASK;
		else if (div < 0)
			div = 0;

		div |= MSD_CLK_SD;
		writel(div, &host->reg->clock_control);
	} else if (!(readl(&host->reg->clock_control) & MSD_CLK_DIS)) {
		/* ensure that the clock is off. */
		writel(readl(&host->reg->clock_control) | MSD_CLK_DIS,
			&host->reg->clock_control);
	}

	if (ios->power_mode == MMC_POWER_OFF) {
		writel(readl(&host->reg->power_control) & ~MSD_SD_POWER_ON,
			&host->reg->power_control);
	} else {
		if (ios->vdd < MIN_POWER)
			power = 0;
		else
			power = ios->vdd - MIN_POWER;

		writel(MSD_SD_POWER_ON | (unsigned int) power,
			&host->reg->power_control);
	}

	if (ios->bus_width == MMC_BUS_WIDTH_1)
		writel(MSD_SINGLE_BUS, &host->reg->bus_width);
	else
		writel(MSD_WIDE_BUS, &host->reg->bus_width);

	spin_unlock_irqrestore(&host->lock, flags);
}

static int moxart_get_ro(struct mmc_host *mmc)
{
	int ret;
	struct moxart_host *host = mmc_priv(mmc);

	(readl(&host->reg->status) & MSD_WRITE_PROT) ? (ret = 1) : (ret = 0);
	return ret;
}

static struct mmc_host_ops moxart_ops = {
	.request = moxart_request,
	.set_ios = moxart_set_ios,
	.get_ro = moxart_get_ro,
};

static int moxart_probe(struct device *dev)
{
	struct mmc_host *mmc;
	struct moxart_host *host = NULL;
	int ret;

	mmc = mmc_alloc_host(sizeof(struct moxart_host), dev);
	if (!mmc) {
		ret = -ENOMEM;
		goto out;
	}

	mmc->ops = &moxart_ops;
	mmc->caps = MMC_CAP_4_BIT_DATA;
	mmc->f_min = 400000;
	mmc->f_max = 25000000;
	mmc->ocr_avail = 0xffff00;	/* support 2.0v - 3.6v power */

	host = mmc_priv(mmc);
	host->mmc = mmc;
	spin_lock_init(&host->lock);
	tasklet_init(&host->card_change_tasklet, moxart_card_change,
		(unsigned long) host);
	tasklet_init(&host->fifo_run_tasklet, moxart_fifo_run,
		(unsigned long) host);
	host->reg = (struct moxart_reg *)IO_ADDRESS(MOXART_MMC_BASE);
	host->dma = apb_dma_alloc(APB_DMA_SD_REQ_NO);

	if (host->dma)
		apb_dma_set_irq(host->dma, moxart_dma_irq, host);


#ifdef MSD_SUPPORT_GET_CLOCK
	unsigned int mul, val, div; /* get system clock */
	mul = (readl(IO_ADDRESS(MOXART_PMU_BASE+0x30)) >> 3) & 0x1ff;
	val = (readl(IO_ADDRESS(MOXART_PMU_BASE+0x0c)) >> 4) & 0x7;
	switch (val) {
	case 0:
		div = 2;
		break;
	case 1:
		div = 3;
		break;
	case 2:
		div = 4;
		break;
	case 3:
		div = 6;
		break;
	case 4:
		div = 8;
		break;
	default:
		div = 2;
		break;
	}
	host->sysclk = (38684*mul + 10000) / (div * 10000);
	host->sysclk = (host->sysclk * 1000000) / 2;
#endif


	/* change I/O multiplexing to SD, so the GPIO 17-10 will be fail */
	moxart_gpio_mp_clear(0xff << 10);

	/* Ensure that the host controller is shut down, and setup with
	   our defaults. disable all interrupt */
	writel(0, &host->reg->interrupt_mask);

	/* reset chip */
	writel(MSD_SDC_RST, &host->reg->command);

	/* wait for reset finished */
	while (readl(&host->reg->command) & MSD_SDC_RST)
		;

	/* disable all interrupt */
	writel(0, &host->reg->interrupt_mask);

	writel(MSD_WIDE_BUS, &host->reg->bus_width);

	moxart_int_set_irq(IRQ_MMC, EDGE, H_ACTIVE);
	ret = request_irq(IRQ_MMC, moxart_irq,
		IRQF_DISABLED, "MOXART MMC", host);

	if (ret)
		goto out;

	/* writel(MSD_INT_CARD_CHANGE|MSD_INT_FIFO_ORUN|MSD_INT_FIFO_URUN,
		&host->reg->interrupt_mask); */
	writel(MSD_INT_CARD_CHANGE, &host->reg->interrupt_mask);
	dev_set_drvdata(dev, mmc);
	mmc_add_host(mmc);

	dev_info(dev, "MOXART MMC: finished moxart_probe\n");
	return 0;

out:
	if (mmc)
		mmc_free_host(mmc);
	return ret;
}

static int moxart_remove(struct device *dev)
{
	struct mmc_host *mmc = dev_get_drvdata(dev);
	struct moxart_host *host = mmc_priv(mmc);

	dev_set_drvdata(dev, NULL);

	if (mmc) {
		mmc_remove_host(mmc);

		if (host->dma) {
			apb_dma_disable(host->dma);
			apb_dma_release_irq(host->dma);
			apb_dma_release(host->dma);
		}
		writel(0, &host->reg->interrupt_mask);
		writel(0, &host->reg->power_control);
		writel(readl(&host->reg->clock_control) | MSD_CLK_DIS,
			&host->reg->clock_control);

		free_irq(IRQ_MMC, host);
		tasklet_kill(&host->card_change_tasklet);
		tasklet_kill(&host->fifo_run_tasklet);

		mmc_free_host(mmc);
	}
	return 0;
}

static struct platform_device moxart_device = {
	.name = "MOXART MMC",
	.id = -1,
};

static struct device_driver moxart_driver = {
	.name = "MOXART MMC",
	.bus = &platform_bus_type,
	.probe = moxart_probe,
	.remove = moxart_remove,
};

static int __init moxart_init(void)
{
	platform_device_register(&moxart_device);
	return driver_register(&moxart_driver);
}

static void __exit moxart_exit(void)
{
	platform_device_unregister(&moxart_device);
	driver_unregister(&moxart_driver);
}

module_init(moxart_init);
module_exit(moxart_exit);

MODULE_DESCRIPTION("MOXART MMC interface driver");
MODULE_LICENSE("GPL");
